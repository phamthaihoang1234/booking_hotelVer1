package com.example.bookinghotel.repositories;


import com.example.bookinghotel.entities.RoomImage;
import org.springframework.data.repository.CrudRepository;

public interface RoomImageRepository extends CrudRepository<RoomImage, Long> {
}
